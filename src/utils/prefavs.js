const prefavsMarkups = {
  none: ``,
  is: `<!-- @1548198204@ -->
  <div ggnoclick id='mGGUID' style='_CLEARCSS_;height:100px;cursor:default;'>
    <br style='display:none;' />
    <style>
      .HYPE_scene {
        background: transparent !important;
      }
  
      .HYPE_element {
        cursor: default;
      }
  
      .cursor_pointer {
        cursor: pointer !important;
      }
    </style>
    <div id='GGUID' style='_CLEARCSS_;height:100px;width:100%;overflow:hidden;'>
      <div id='[macro_HYPE_CONTAINER]_hype_container' style='margin:auto;position:relative;width:100%;height:100%;overflow:hidden;'
           aria-live='polite'>
        <script src='[macro_HYPE_URL]' defer></script>
      </div>
    </div>
    <script>
      var G = (window.GUMGUM || parent.GUMGUM || top.GUMGUM);
      G.onEvent(document.body, 'click', '.cursor_pointer', openAd);
  
      function ggStart() {
        G.log('[adBuyId]: ggStart');
      }
  
      function openAd(e) {
        G.loadImg("[clickUrl]");
        window.open("[macro_DSP_MACRO][macro_CLICK_TRACKER]", "_blank");
      }
    </script>
    <!--begintags-->
    [macro_IMPRESSIONS]
    <!--endtags-->
  </div>
  `,
  ii: `<!-- @1548197726@ -->
    <div ggnoclick id='mGGUID' style='_CLEARCSS_;height:100px;cursor:default;'>
      <br style='display:none;' />
      <style>
        .HYPE_scene {
          background: transparent !important;
        }
    
        .HYPE_element {
          cursor: default;
        }
    
        .cursor_pointer {
          cursor: pointer !important;
        }
      </style>
      <div id='GGUID' style='_CLEARCSS_;height:100px;width:100%;overflow:hidden;'>
        <div id='[macro_HYPE_CONTAINER]_hype_container' style='margin:auto;position:relative;width:100%;height:100%;overflow:hidden;'
             aria-live='polite'>
          <script src='[macro_HYPE_URL]' defer></script>
        </div>
      </div>
      <script>
        var G = (window.GUMGUM || parent.GUMGUM || top.GUMGUM);
        G.onEvent(document.body, 'click', '.cursor_pointer', openAd);
    
        function ggStart() {
          G.log('[adBuyId]: ggStart');
        }
    
        function openAd(e) {
          G.loadImg("[clickUrl]");
          window.open("[macro_DSP_MACRO][macro_CLICK_TRACKER]", "_blank");
        }
      </script>
      <!--begintags-->
      [macro_IMPRESSIONS]
      <!--endtags-->
    </div>
    `,
  isxv: `<!-- @1591821899@ -->
    <div ggnoclick id="mGGUID" style="_CLEARCSS_;height:100px;cursor:default;">
      <br style="display: none" />
      <style>
        #GGUID_CONTAINER {
          transform-origin: center bottom !important;
          overflow: visible !important;
          font-size: 0 !important;
        }
        #GGUID,
        #mGGUID {
          transform-origin: center bottom !important;
          overflow: visible !important;
        }
        .HYPE_scene {
          overflow: visible !important;
          background: transparent !important;
        }
        .HYPE_element {
          cursor: default;
        }
        .cursor_pointer {
          cursor: pointer !important;
        }
        .gg-ez-vp--controls {
          pointer-events: all !important;
        }
      </style>
      <script src="https://c.gumgum.com/vp/latest/gg-ez-vp.js"></script>
      <link rel="stylesheet" href="https://c.gumgum.com/vp/latest/gg-ez-vp.css" />
      <div id="GGUID" style="_CLEARCSS_;height:100px;width:100%;overflow:hidden;">
        <div
          id="[macro_HYPE_CONTAINER]_hype_container"
          class="HYPE_document"
          style="
              margin: auto;
              position: absolute;
              bottom: 0px;
              left: 0px;
              width: 100%;
              height: 100%;
              overflow: visible;
            "
        >
          <script
            type="text/javascript"
            charset="utf-8"
            src="[macro_HYPE_URL]"
          ></script>
        </div>
      </div>
      <script>
        var G = window.GUMGUM || parent.GUMGUM || top.GUMGUM;
        var WRAPPER = window.frameElement || document.getElementById("root");
        WRAPPER.setAttribute("height", 100);
        WRAPPER.style.height = "100px";
        if (G && G.isad) {
          G.isad.container.style.height = "100px";
        }
        var HD;
        var state = {
          player: null,
          isVAST: [macro_isVAST],
          videoUrl: "[macro_VIDEO_URL]",
        };
        G.onEvent(
          document.body,
          "click",
          ".cursor_pointer, .gg-ez-vp--blocker",
          openAd
        );
        function ggInitVideo() {
          var breakpoint = "600";
          if (window.innerWidth < 600) breakpoint = "300";
          console.log(breakpoint);
          var containerId = "gg_video_" + breakpoint;
          var videoUrl = state.videoUrl;
          startEmbeddedVideoFn(videoUrl, containerId);
        }
        function startEmbeddedVideoFn(vid, containerId) {
          console.log("loading video: " + vid, containerId);
          var video = vid;
          // Reset player if exists
          if (state.player) {
            state.player.destroy();
          }
          var container = document.getElementById(containerId);
          if (!video || !container) return;
          var ggPlayer = null;
          var ggVideoPlayFired = false;
          var ggVideo25Fired = false;
          var ggVideo50Fired = false;
          var ggVideo75Fired = false;
          embedVideo();
          function embedVideo() {
            ggVideoPlayFired = ggVideo25Fired = ggVideo50Fired = ggVideo75Fired = false;
            ggPlayer = new GgEzVp({
              container: container,
              src: vid,
              width: "100%",
              isVAST: state.isVAST,
              controls: {
                timestamp: false,
                progress: false,
                play: false,
                expand: false,
                volumeRange: false,
                volume: true,
              },
            });
            state.player = ggPlayer;
            ggPlayer.once("play", function() {
              G.loadImg("[videoAutoplayImpressionUrl]");
              console.log("videoAutoplayImpression");
              G.loadImg("[videoAutoplayPlayUrl]");
              console.log("videoAutoplayPlay");
              ggVideoPlayFired = true;
            });
            ggPlayer.on("player-click", function() {
              ggPlayer.volume(0);
            });
            ggPlayer.on("ready", function() {
              ggPlayer.play();
            });
            ggPlayer.once("ended", function() {
              if (ggVideo75Fired) {
                G.loadImg("[videoAutoplayCompleted100Url]");
                console.log("videoAutoplayCompleted100");
              }
            });
            ggPlayer.on("playback-progress", function(e) {
              var currentTime = e.currentTime;
              var duration = e.duration;
              if (!ggVideo25Fired && currentTime >= duration * 0.25) {
                G.loadImg("[videoAutoplayCompleted25Url]");
                console.log("videoAutoplayCompleted25");
                ggVideo25Fired = true;
              }
              if (!ggVideo50Fired && currentTime >= duration * 0.5) {
                G.loadImg("[videoAutoplayCompleted50Url]");
                console.log("videoAutoplayCompleted50");
                ggVideo50Fired = true;
              }
              if (!ggVideo75Fired && currentTime >= duration * 0.75) {
                G.loadImg("[videoAutoplayCompleted75Url]");
                console.log("videoAutoplayCompleted75");
                ggVideo75Fired = true;
              }
            });
          }
        }
        function ggMuteVideo() {
          state.player.muteUnmute();
        }
        function ggStart() {
          G.log("[adBuyId]: ggStart");
        }
        function ggFinish() {
          G.log("[adBuyId]: ggFinish");
        }
        function setScale(type) {
          var iframeScale = "";
          var adContainerScale = "";
          var containerScale = "";
          mobileMode = G.isMobile();
          switch (type) {
            case "up":
              iframeScale = "scale(1,2)";
              adContainerScale = "scale(1, 0.5)";
              containerScale = "scale(1,1)";
              break;
            case "down":
              iframeScale = "scale(1,1)";
              adContainerScale = "initial";
              containerScale = "initial";
              break;
            default:
              break;
          }
          if (GUMGUMAD) {
            GUMGUMAD.iframe.style.transformOrigin = "bottom";
            GUMGUMAD.iframe.style.transform = iframeScale;
          } else {
            parent.window.document.getElementById(
              "GGUID_ifr"
            ).style.transform = iframeScale;
            parent.window.document.getElementById(
              "GGUID_ifr"
            ).style.transformOrigin = "bottom";
          }
          HD && HD.relayoutIfNecessary();
          document.getElementById("mGGUID").style.transform = adContainerScale;
          document.getElementById("GGUID").style.transform = containerScale;
        }
        function ggExpand(e) {
          console.log("expand!");
          G.loadImg("[otherEvent1Url]");
          setScale("up");
        }
        function ggCollapse(e) {
          console.log("collapse");
          setScale("down");
        }
        function openAd(e) {
          G.loadImg("[clickUrl]");
          window.open("[macro_DSP_MACRO][macro_CLICK_TRACKER]", "_blank");
          if (state.player && !state.player.player.muted) {
            // plays mute animation if video was unmuted
            if (HD) {
              var muteSymbol = HD.getSymbolInstancesByName("Mute");
              muteSymbol.forEach(function(el) {
                el.startTimelineNamed("Main Timeline", HD.kDirectionForward);
              });
            }
            state.player.mute();
          }
        }
        // to use hype method "relayoutIfNecessary()" to smooth out animation
        function handleLoad(hypeDocument, element, event) {
          if (element.id === "[macro_HYPE_CONTAINER]_hype_container") {
            HD = hypeDocument;
          }
        }
        HYPE_eventListeners = window.HYPE_eventListeners || [];
        window.HYPE_eventListeners.push({
          type: "HypeDocumentLoad",
          callback: handleLoad,
        });
      </script>
      <!--begintags-->
      [macro_IMPRESSIONS]
      <!--endtags-->
    </div>
    `,
  isx: `<!-- @1591821899@ -->
  <div ggnoclick id='mGGUID' style='_CLEARCSS_;height:100px;cursor:default;'>
    <br style='display: none' />
    <style>
      #GGUID_CONTAINER {
        transform-origin: center bottom !important;
        overflow: visible !important;
        font-size: 0 !important;
      }
      #GGUID,
      #mGGUID {
        transform-origin: center bottom !important;
        overflow: visible !important;
      }
      .HYPE_scene {
        overflow: visible !important;
        background: transparent !important;
      }
      .HYPE_element {
        cursor: default;
      }
      .cursor_pointer {
        cursor: pointer !important;
      }
    </style>
    <div id='GGUID' style='_CLEARCSS_;height:100px;width:100%;overflow:hidden;'>
      <div
        id='[macro_HYPE_CONTAINER]_hype_container'
        class='HYPE_document'
        style='
          margin: auto;
          position: absolute;
          bottom: 0px;
          left: 0px;
          width: 100%;
          height: 100%;
          overflow: visible;
        '
      >
        <script
          type='text/javascript'
          charset='utf-8'
          src='[macro_HYPE_URL]'
        ></script>
      </div>
    </div>
    <script>
      var G = window.GUMGUM || parent.GUMGUM || top.GUMGUM;
      var WRAPPER = window.frameElement || document.getElementById('root');
      var HD;
      G.onEvent(
        document.body,
        'click',
        '.cursor_pointer',
        openAd
      );
      function ggStart() {
        G.log('[adBuyId]: ggStart');
      }
      function ggFinish() {
        G.log('[adBuyId]: ggFinish');
      }
      function setScale(type) {
        var iframeScale = '';
        var adContainerScale = '';
        var containerScale = '';
        mobileMode = G.isMobile();
        switch (type) {
          case 'up':
            iframeScale = 'scale(1,2)';
            adContainerScale = 'scale(1, 0.5)';
            containerScale = 'scale(1,1)';
            break;
          case 'down':
            iframeScale = 'scale(1,1)';
            adContainerScale = 'initial';
            containerScale = 'initial';
            break;
          default:
            break;
        }
        if (GUMGUMAD) {
          GUMGUMAD.iframe.style.transformOrigin = 'bottom';
          GUMGUMAD.iframe.style.transform = iframeScale;
        } else {
          parent.window.document.getElementById('GGUID_ifr').style.transform = iframeScale;
          parent.window.document.getElementById( 'GGUID_ifr').style.transformOrigin = 'bottom';
        }
        HD && HD.relayoutIfNecessary();
        document.getElementById('mGGUID').style.transform = adContainerScale;
        document.getElementById('GGUID').style.transform = containerScale;
      }
      function ggExpand(e) {
        console.log('expand!');
        G.loadImg('[otherEvent1Url]');
        setScale('up');
      }
      function ggCollapse(e) {
        console.log('collapse');
        setScale('down');
      }
      function openAd(e) {
        G.loadImg("[clickUrl]");
        window.open("[macro_DSP_MACRO][macro_CLICK_TRACKER]", "_blank");
      }
      // to use hype method 'relayoutIfNecessary()' to smooth out animation
      function handleLoad(hypeDocument, element, event) {
        if (element.id === '[HYPE_CONTAINER]_hype_container') {
          HD = hypeDocument;
        }
      }
      HYPE_eventListeners = window.HYPE_eventListeners || [];
      window.HYPE_eventListeners.push({
        type: 'HypeDocumentLoad',
        callback: handleLoad,
      });
    </script>
    <!--begintags-->
    [macro_IMPRESSIONS]
    <!--endtags-->
  </div>`,
};

export default prefavsMarkups;
