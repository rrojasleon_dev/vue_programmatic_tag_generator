import * as XLSX from "xlsx";

class utils {
  parseCSV(csvFile) {
    const fileReader = new FileReader();
    return new Promise((resolve) => {
      fileReader.onload = (e) => {
        const fileData = e.target.result;
        const lines = fileData.split('\n');
        const header = lines[0].split(',')
        const output = lines.slice(1).map(line => {
          const fields = line.split(',')
          return Object.fromEntries(header.map((h, i) => [h, fields[i]]));
        });
        const finalJSON =  JSON.parse(JSON.stringify(output));
        resolve(finalJSON);
      };

      fileReader.readAsText(csvFile);
    });
  }

  parseExcel(excelFile) {
    const reader = new FileReader();
    return new Promise((resolve) => {
      reader.onload = (evt) => {
        // evt = on_file_select event
        /* Parse data */
        const bstr = evt.target.result;
        const wb = XLSX.read(bstr, { type: "binary" });
        /* Get first worksheet */
        const wsname = wb.SheetNames[0];
        const ws = wb.Sheets[wsname];
        /* Convert array of arrays */
        const data = XLSX.utils.sheet_to_json(ws);
        
        resolve(data);
      };
      reader.readAsBinaryString(excelFile);
    });
  }
}

export default new utils();
