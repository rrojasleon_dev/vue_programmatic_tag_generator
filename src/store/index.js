import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules';


Vue.use(Vuex)

export default new Vuex.Store({
  modules,

  actions: {
    reset({commit}) {
      return new Promise(resolve => {
        Object.keys(modules).forEach(moduleName => {
          console.log('RESET');
          
          commit(`${moduleName}/RESET`);
        });
        resolve();
      });
    }
  }
});
