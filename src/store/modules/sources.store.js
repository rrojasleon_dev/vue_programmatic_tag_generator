const state = {
    sources: []
};

const getters = {

    /**
     * Return sources array
     * @param state
     * @returns {*}
     */
    sources: state => state.sources
};

const actions = {
    addSingle({commit}, payload) {
        return new Promise((resolve) => {
            commit('ADD_SINGLE', payload);
            resolve();
        });
    },

    reset({commit}) {
        commit('RESET', false);
    }
};

const mutations = {
    ADD_SINGLE(state, payload) {
        const newSource = {
            name: payload.name,
            data: payload.data
        };

        state.sources.push(newSource);
    },

    RESET(state) {
        state.sources = [];
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};