import prefavsMarkups from "../../utils/prefavs";

const state = {
  templates: [],
  prefavs: [
    { label: "-- None --", value: "none" },
    { label: "IS", value: "is" },
    { label: "ISX", value: "isx" },
    { label: "ISXV", value: "isxv" },
    { label: "II", value: "ii" },
  ],
};

const getters = {
  /**
   * Return templates array
   * @param state
   * @returns {*}
   */
  templates: (state) => state.templates,

  /**
   * Return templates array
   * @param state
   * @returns {*}
   */
  prefavs: (state) => state.prefavs,
};

const actions = {
  addSingle({ commit }) {
    return new Promise((resolve) => {
      commit("ADD_SINGLE");
      resolve();
    });
  },

  selectPrefav({ commit }, payload) {
    return new Promise((resolve) => {
      const data = {
        markup: prefavsMarkups[payload.selected],
        templateIndex: payload.templateIndex,
      };

      commit("UPDATE_SINGLE_MARKUP", data);
      resolve();
    });
  },

  updateSingleMarkup({ commit }, payload) {
    const data = {
      markup: payload.newMarkup,
      templateIndex: payload.templateIndex,
    };
    return new Promise((resolve) => {
      commit("UPDATE_SINGLE_MARKUP", data);
      resolve();
    });
  },

  removeSingle({ commit }, templateIndex) {
    return new Promise((resolve) => {
      commit("REMOVE_SINGLE", templateIndex);
      resolve();
    });
  },

  setMacros({ commit }, sources) {
    return new Promise((resolve) => {
      commit("SET_MACROS", sources);
      resolve();
    });
  },

  addMacro({ commit }, templateIndex) {
    commit("ADD_CUSTOM_MACRO", templateIndex);
  },

  removeMacro({commit}, payload) {
    commit('REMOVE_MACRO', payload);
  },

  reset({ commit }) {
    commit("RESET", false);
  },
};

const mutations = {
  ADD_SINGLE(state) {
    const newTpl = {
      name: "Template Name",
      content: "",
      prefav: "none",
      macros: [],
      identifier: {
        source: "",
        sourceKey: ""
      }
    };

    state.templates.push(newTpl);
  },

  UPDATE_SINGLE_MARKUP(state, payload) {
    state.templates[payload.templateIndex].content = payload.markup;
  },

  REMOVE_SINGLE(state, index) {
    state.templates.splice(index, 1);
  },

  SET_MACROS(state) {
    const macroRegex = new RegExp(/\[(macro_.*?)\]/g);
    state.templates.forEach((tpl) => {
      const matches = tpl.content.match(macroRegex);
      if (matches !== null) {
        matches.forEach((macroName) => {
          tpl.macros.push({
            macroName: macroName,
            replaceWith: {
              sourceName: "",
              sourceKey: "",
            },
          });
        });
      }
    });
  },

  ADD_CUSTOM_MACRO(state, templateIndex) {
    state.templates[templateIndex].macros.push({
      macroName: "",
      replaceWith: {
        sourceKey: "",
        sourceName: ""
      }
    });
  },

  REMOVE_MACRO(state, payload) {
    state.templates[payload.tplIndex].macros.splice(payload.macroIndex, 1);
  },

  RESET(state) {
    console.log("reset");
    state.templates = [];
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
